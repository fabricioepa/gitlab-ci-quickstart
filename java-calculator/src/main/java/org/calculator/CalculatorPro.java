package org.calculator;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.calculator.history.CalculatorHistory;
import org.calculator.history.OperationEntry;

/**
 * Extends basic calculator to support pro features.
 */
public class CalculatorPro extends Calculator {

	private CalculatorHistory history;

	@Override
	public int sum(int a, int b) {
		addToHistory(new OperationEntry("sum", a, b));
		return super.sum(a, b);
	}

	@Override
	public int sub(int a, int b) {
		addToHistory(new OperationEntry("sub", a, b));
		return super.sub(a, b);
	}

	private void addToHistory(OperationEntry entry) {
		if (history != null) {
			this.history.add(entry);
		}
	}

	// ----- Featured methods ------ //

	public void setHistory(CalculatorHistory history) {
		this.history = history;
	}

	public CalculatorHistory getHistory() {
		return history;
	}

	public void exportHistoryToOperatingSystemFile(File file) throws IOException {
		if (this.history != null) {
			FileWriter fw = new FileWriter(file);
			for (OperationEntry entry : this.history.getEntries()) {
				fw.write(entry.toString());
			}
			fw.flush();
			fw.close();
		}
	}

}
