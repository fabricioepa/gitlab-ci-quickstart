package org.calculator.history;

import java.util.Arrays;

/**
 * This class encapsulates the details for a given calculator method.
 */
public class OperationEntry {

	private final Object[] arguments;
	private final String operation;

	public OperationEntry(String operation, Object... args) {
		this.operation = operation;
		this.arguments = args;
	}

	public Object[] getArguments() {
		return arguments;
	}

	public String getOperation() {
		return operation;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(arguments);
		result = prime * result
				+ ((operation == null) ? 0 : operation.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OperationEntry other = (OperationEntry) obj;
		if (!Arrays.equals(arguments, other.arguments))
			return false;
		if (operation == null) {
			if (other.operation != null)
				return false;
		} else if (!operation.equals(other.operation))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "OperationEntry [operation=" + operation + ", arguments="
				+ Arrays.toString(arguments) + "] \n";
	}

}
