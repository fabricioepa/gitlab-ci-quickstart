package org.calculator.history;

import java.util.ArrayList;
import java.util.List;

/**
 * This class manages all history entries for a given calculator instance.
 */
public class CalculatorHistory {

	private final List<OperationEntry> entries = new ArrayList<OperationEntry>();

	public void add(OperationEntry entry) {
		this.entries.add(entry);
	}

	public List<OperationEntry> getEntries() {
		return this.entries;
	}

	public void clear() {
		this.entries.clear();
	}

}
