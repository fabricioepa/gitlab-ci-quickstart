package org.calculator;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.calculator.CalculatorPro;
import org.calculator.history.CalculatorHistory;
import org.calculator.history.OperationEntry;
import org.junit.Test;
import org.junit.experimental.categories.Category;

/**
 * <p>
 * Integration Test Level will grant the side-effects are under control and all
 * software components work together as expected. These tests may also
 * communicate with external softwares such as: databases, web services,
 * operating system, etc.
 */
public class CalculatorIntegrationTest {

	@Test
	@Category(value = CalculatorIntegrationTest.class)
	public void testOperationHistory() {
		CalculatorPro calc = new CalculatorPro();

		assertNull(calc.getHistory());

		calc.setHistory(new CalculatorHistory());

		assertEquals(3, calc.sum(1, 2));
		assertEquals(-2, calc.sub(-1, 1));
		assertEquals(-1, calc.sum(0, -1));

		List<OperationEntry> entries = calc.getHistory().getEntries();
		assertEquals(new OperationEntry("sum", 1, 2), entries.get(0));
		assertEquals(new OperationEntry("sub", -1, 1), entries.get(1));
		assertEquals(new OperationEntry("sum", 0, -1), entries.get(2));

		System.out.println("testOperationHistory()...");
		System.out.println(entries);
	}

	@Test
	@Category(value = CalculatorIntegrationTest.class)
	public void testExportHistoryToOperatingSystemFile() {
		CalculatorPro calc = new CalculatorPro();
		calc.setHistory(new CalculatorHistory());
		assertEquals(3, calc.sum(1, 2));
		assertEquals(0, calc.sub(2, 2));
		assertEquals(7, calc.sum(3, 4));

		try {
			File log = new File("target/test-log.txt");

			assertFalse(log.exists());

			calc.exportHistoryToOperatingSystemFile(log);

			assertTrue(log.exists() && log.canRead());
		} catch (IOException e) {
			fail(e.getMessage());
			e.printStackTrace();
		}
	}

}
