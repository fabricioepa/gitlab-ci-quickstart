package org.calculator;

import static org.junit.Assert.*;

import org.calculator.Calculator;
import org.junit.Test;
import org.junit.experimental.categories.Category;

public class CalculatorUnitTest {

	@Test
	@Category(CalculatorUnitTest.class)
	public void testSumMethod() {
		Calculator calc = new Calculator();

		assertEquals(3, calc.sum(1, 2));
		assertEquals(0, calc.sum(-1, 1));
		assertEquals(-1, calc.sum(0, -1));
		assertEquals(-1, calc.sum(-1, 0));
	}

	@Test
	@Category(CalculatorUnitTest.class)
	public void testSubMethod() {
		Calculator calc = new Calculator();

		assertEquals(1, calc.sub(1, 0));
		assertEquals(-1, calc.sub(0, 1));
		assertEquals(0, calc.sub(0, 0));
		assertEquals(0, calc.sub(2, 2));
	}

}
