# GitLab CI Project

A continuous integration quickstart project.

## 1) Enable the GitLab CI as your orchestration tool

Go to **Settings > Services > GitLab CI** and activate it.

## 2) Installing **GitLab CI Runner** as a service in your virtual test environment

Go to the link and follow the instructions

https://gitlab.com/gitlab-org/gitlab-ci-multi-runner/blob/master/docs/install/windows.md

## 2) Create automation scripts in the project source code on repository

See sample script files in *scripts/* folder:

* `build.cmd`
* `test.cmd`

## 3) Define the workflow based on stages

* a. Write the **.gitab-ci.yml** file in the root project dir
* b. Configure the **stages** you want for your pipeline
* c. Associate them to the automation scripts.

Improve testing process by integrating automated tests and QA metrics in the pipeline.

For more training materials, please visit:

http://fabricioepa.wordpress.com