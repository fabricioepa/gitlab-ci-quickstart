@echo off
@REM Author: fabricio.epaminondas
@REM Description: Windows batch scripting for running the tests for Java Calculator project 

echo Setting up the environment for the integration tests
echo.

echo Cleaning previous generated calculator history files on disk...
echo ...to avoid test conflict
call mvn clean -f java-calculator
