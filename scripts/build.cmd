@echo off
@REM Author: fabricio.epaminondas
@REM Description: Windows batch scripting for building the Java Calculator project 

echo Building java calculator project

call mvn clean compile -f java-calculator 
