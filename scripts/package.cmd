@echo off
@REM Author: fabricio.epaminondas
@REM Description: Windows batch scripting for running the tests for Java Calculator project 

echo Creating the project package for distribution

call mvn package -DskipTests -f java-calculator
