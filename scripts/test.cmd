@echo off
@REM Author: fabricio.epaminondas
@REM Description: Windows batch scripting for running the tests for Java Calculator project 

echo Executing Unit Tests...

echo ..the first level of testing

call mvn test -Dgroups=org.calculator.CalculatorUnitTest -f java-calculator
