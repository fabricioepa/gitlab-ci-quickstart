@echo off
@REM Author: fabricio.epaminondas
@REM Description: Windows batch scripting for running the tests for Java Calculator project 

echo Executing the integration test level

call mvn integration-test -Dgroups=org.calculator.CalculatorIntegrationTest -f java-calculator

